--[[
	Watto
	Version 1.3
]]

Watto_Version = "1.3";

local Watto_Color_01 = "|CFF57a5bb"; -- Standard Watto Color
local Watto_Color_02 = "|CFF8dd2e6"; -- Light Watto Color

Watto_Realm = GetRealmName();
Watto_Player = UnitName("player");
Watto_Buy_CurrentItem = nil;
Watto_Buy_CurrentButton = nil;

local Watto_Buy_FirstCall = false;
local Watto_Count = 0;

Watto_TempSell = {};

Watto_Defaults = {
	["Options"] = {
		["version"] = Watto_Version,
		["autosell"] = "off",
		["sellnotify"] = "on",
		["randomselltext"] = "on",
		["autosellfood"] = "on",
	},
};

Watto_Food_NoAutosell = {
	6522,		-- Deviate Fish
	6657,		-- Savory Deviate Delight
	19221,		-- Darkmoon Special Reserve
	44114,		-- Old Spices
	44228,		-- Baby Spice
};

StaticPopupDialogs["WATTO_CONFIRMPURCHASETOKENITEM"] = {
	text = CONFIRM_PURCHASE_TOKEN_ITEM,
	button1 = YES,
	button2 = NO,
	OnAccept = function()
		Watto_Purchase(Watto_Count);
		return;
	end,
	OnCancel = function()
		return;
	end,
	OnShow = function()
	end,
	OnHide = function()
	end,
	timeout = 0,
	hideOnEscape = 1,
	hasItemFrame = 1,
}

--[[ Add in missing functions from later WoW versions ]] --

-- Concat the contents of the parameter list,
-- separated by the string delimiter (just like in perl)
-- example: strjoin(", ", {"Anna", "Bob", "Charlie", "Dolores"})
function strjoin(delimiter, list)
  local len = getn(list)
  if len == 0 then 
    return "" 
  end
  local string = list[1]
  for i = 2, len do 
    string = string .. delimiter .. list[i] 
  end
  return string
end

-- Split text into a list consisting of the strings in text,
-- separated by strings matching delimiter (which may be a pattern). 
-- example: strsplit(",%s*", "Anna, Bob, Charlie,Dolores")
function strsplit(delimiter, text)
  local list = {}
  local pos = 1
  if strfind("", delimiter, 1) then -- this would result in endless loops
    error("delimiter matches empty string!")
  end
  while 1 do
    local first, last = strfind(text, delimiter, pos)
    if first then -- found?
      tinsert(list, strsub(text, pos, first-1))
      pos = last+1
    else
      tinsert(list, strsub(text, pos))
      break
    end
  end
  return list
end

function IsEquippableItem(ItemID)
	if not ItemID then return false end
	local _,_,_,_,_,_,_,equip,_ = GetItemInfo(ItemID)
	if equip ~= "" then
		return false
	else
		return true
	end
end

--[[ Done Adding in missing WoW functions ]]--


function Watto_Msg(msg,channel)
	if (msg == nil) then return; end
	if channel ~= nil then
		channel = strlower(channel);
	end

	if (channel == "error") then
		DEFAULT_CHAT_FRAME:AddMessage("|cffff2020"..WATTO_TITLE.." "..WATTO_ERROR..": |r"..msg);
	else
		DEFAULT_CHAT_FRAME:AddMessage(Watto_Color_01..WATTO_TITLE..": |r"..msg);
	end
end

function Watto_Load(self)
	self:RegisterEvent("VARIABLES_LOADED");
	self:RegisterEvent("MERCHANT_SHOW");

	SlashCmdList["Watto"] = Watto_CommandLine;
	SLASH_Watto1 = "/watto";
end

local orig_MerchantFrame_Update
local orig_CloseMerchant
local Orig_MerchantItemButton_OnModifiedClick

function Watto_Loaded()
	Watto_Database_Build();
	Watto_Database_Update();

	orig_MerchantFrame_Update = MerchantFrame_Update
	MerchantFrame_Update = Watto_Merchant_ChangeTab
	orig_CloseMerchant = CloseMerchant
	CloseMerchant = Watto_Merchant_OnHide
	Orig_MerchantItemButton_OnModifiedClick = MerchantItemButton_OnModifiedClick;
	MerchantItemButton_OnModifiedClick = Watto_Buy_OnModClick;

	Watto_Msg("Loaded.");
end

--== Database Building Functions ==--
function Watto_Database_Build()
	if (Watto_Options) and (Watto_Options[Watto_Realm]) and (Watto_Options[Watto_Realm][Watto_Player]) and (Watto_Options[Watto_Realm][Watto_Player]["Version"]) then
		if (Watto_Options[Watto_Realm][Watto_Player]["Version"] < "1.4") then
			Watto_Options[Watto_Realm][Watto_Player] = nil;
			Watto_Msg("Your Watto Options can not be updated and have been deleted.");
		end
	end

	if (not Watto_Options) then
		Watto_Options = {};
	end
	if (not Watto_Options[Watto_Realm]) then
		Watto_Options[Watto_Realm] = {};
	end
	if (not Watto_Options[Watto_Realm][Watto_Player]) then
		Watto_Options[Watto_Realm][Watto_Player] = Watto_Defaults["Options"];
	end
	if (not Watto_ItemList) then
		Watto_ItemList = {
			["General"] = {},
			["PerChar"] = {},
		}
	end
	if (not Watto_ItemList["PerChar"][Watto_Realm]) then
		Watto_ItemList["PerChar"][Watto_Realm] = {};
	end
	if (not Watto_ItemList["PerChar"][Watto_Realm][Watto_Player]) then
		Watto_ItemList["PerChar"][Watto_Realm][Watto_Player] = {};
	end
end

--== Database Updating Functions ==--
function Watto_Database_Update()
	-- If No Databases
	if (not Watto_Options) or (not Watto_ItemList) then
		Watto_Database_Build();
		return;
	end
	-- If no Version Number
	if (not Watto_Options[Watto_Realm][Watto_Player]["Version"]) then
		Watto_Database_Build();
		return;
	end
	-- If Version Number is the Current Version
	if (Watto_Options[Watto_Realm][Watto_Player]["Version"] == Watto_Version) then
		return;
	end

	-- Else, Update the Database!
	Watto_Msg("Updating Database.");

	local temp = Watto_Defaults["Options"];
	local db = Watto_Options[Watto_Realm][Watto_Player];

	-- Update Options function
	for k,v in pairs(db) do
		if (type(temp[k])=="table") and (type(db[k]) == "table") then
			for k2,v2 in pairs(db[k]) do
				if (type(temp[k][k2])=="table") and (type(db[k][k2]) == "table") then
					for k3,v3 in pairs(db[k][k2]) do
						if (type(temp[k][k2][k3])=="table") and (type(db[k][k2][k3]) == "table") then
							for k4,v4 in pairs(db[k][k2][k3]) do
								if (type(temp[k][k2][k3][k4])=="table") and (type(db[k][k2][k3][k4]) == "table") then
									for k5,v5 in pairs(db[k][k2][k3][k4]) do
										if ((db[k][k2][k3][k4][k5]) and (v5 ~= nil)) then
											temp[k][k2][k3][k4][k5] = v5;
										end
									end
								elseif ((db[k][k2][k3][k4]) and (v4 ~= nil)) then
									temp[k][k2][k3][k4] = v4;
								end
							end
						elseif ((db[k][k2][k3]) and (v3 ~= nil)) then
							temp[k][k2][k3] = v3;
						end
					end
				elseif ((db[k][k2]) and (v2 ~= nil)) then
					temp[k][k2] = v2;
				end
			end
		elseif ((db[k]) and (v ~= nil)) then
			temp[k] = v;
		end
	end

	temp["Version"] = Watto_Version;
	Watto_Options[Watto_Realm][Watto_Player] = {};
	Watto_Options[Watto_Realm][Watto_Player] = temp;
end

function Watto_Events(self, event, ...)
	if (event =="VARIABLES_LOADED") then
		Watto_Loaded();
	elseif (event == "MERCHANT_SHOW") then
		Watto_Merchant_OnShow();
	end
end

function Watto_CommandLine(cmd)
	-- Force lowercase so we don't have to double-up our options! Also makes it more user-proof.
	if cmd == nil then cmd = ""; end
	cmd = strlower(cmd);

	--Watto_Msg("Command: \""..cmd.."\"");

	if cmd == "" then
		Watto_Msg(WATTO_HELP);
	elseif cmd == WATTO_CMD_ADD then
		Watto_Msg(WATTO_HELP_ADD_TEXT);
	elseif cmd == WATTO_CMD_ADD_ME then
		Watto_Msg(WATTO_HELP_ADD_ME_TEXT);
	elseif cmd == WATTO_CMD_REM or cmd == WATTO_CMD_REMOVE then
		Watto_Msg(WATTO_HELP_REM_TEXT);
	elseif cmd == WATTO_CMD_REM_ME or cmd == WATTO_CMD_REMOVE_ME then
		Watto_Msg(WATTO_HELP_REM_ME_TEXT);
	elseif cmd == WATTO_CMD_REM_ALL or cmd == WATTO_CMD_REMOVE_ALL then
		Watto_ItemList["General"] = {};
		Watto_Msg(WATTO_LIST_REMALL);
	elseif cmd == WATTO_CMD_REM_ME_ALL or cmd == WATTO_CMD_REMOVE_ME_ALL then
		Watto_ItemList["PerChar"][Watto_Realm][Watto_Player] = {};
		Watto_Msg(WATTO_LIST_REMMEALL);
	elseif cmd == WATTO_CMD_OPTIONS_AUTOSELL then
		if (Watto_Options[Watto_Realm][Watto_Player]["autosell"] == "on") then
			Watto_Options[Watto_Realm][Watto_Player]["autosell"] = "off";
			Watto_Msg(WATTO_CMDTXT_OPTIONS_AUTOSELL_OFF);
		else
			Watto_Options[Watto_Realm][Watto_Player]["autosell"] = "on";
			Watto_Msg(WATTO_CMDTXT_OPTIONS_AUTOSELL_ON);
		end
	elseif cmd == WATTO_CMD_OPTIONS_AUTOSELL_ON then
		Watto_Options[Watto_Realm][Watto_Player]["autosell"] = "on";
		Watto_Msg(WATTO_CMDTXT_OPTIONS_AUTOSELL_ON);
	elseif cmd == WATTO_CMD_OPTIONS_AUTOSELL_OFF then
		Watto_Options[Watto_Realm][Watto_Player]["autosell"] = "off";
		Watto_Msg(WATTO_CMDTXT_OPTIONS_AUTOSELL_OFF);
	elseif cmd == WATTO_CMD_OPTIONS_AUTOSELL_FOOD_ON then
		Watto_Options[Watto_Realm][Watto_Player]["autosellfood"] = "on";
		Watto_Msg(WATTO_CMDTXT_OPTIONS_AUTOSELLFOOD_ON);
	elseif cmd == WATTO_CMD_OPTIONS_AUTOSELL_FOOD_OFF then
		Watto_Options[Watto_Realm][Watto_Player]["autosellfood"] = "off";
		Watto_Msg(WATTO_CMDTXT_OPTIONS_AUTOSELLFOOD_OFF);
	elseif cmd == WATTO_CMD_OPTIONS_SELLNOTIFY then
		if (Watto_Options[Watto_Realm][Watto_Player]["sellnotify"] == "on") then
			Watto_Options[Watto_Realm][Watto_Player]["sellnotify"] = "off";
			Watto_Msg(WATTO_CMDTXT_OPTIONS_SELLNOTIFY_OFF);
		else
			Watto_Options[Watto_Realm][Watto_Player]["sellnotify"] = "on";
			Watto_Msg(WATTO_CMDTXT_OPTIONS_SELLNOTIFY_ON);
		end
	elseif cmd == WATTO_CMD_OPTIONS_SELLNOTIFY_ON then
		Watto_Options[Watto_Realm][Watto_Player]["sellnotify"] = "on";
		Watto_Msg(WATTO_CMDTXT_OPTIONS_SELLNOTIFY_ON);
	elseif cmd == WATTO_CMD_OPTIONS_SELLNOTIFY_OFF then
		Watto_Options[Watto_Realm][Watto_Player]["sellnotify"] = "off";
		Watto_Msg(WATTO_CMDTXT_OPTIONS_SELLNOTIFY_OFF);
	elseif cmd == WATTO_CMD_OPTIONS_RANDOMSELLTEXT then
		if (Watto_Options[Watto_Realm][Watto_Player]["randomselltext"] == "on") then
			Watto_Options[Watto_Realm][Watto_Player]["randomselltext"] = "off";
			Watto_Msg(WATTO_CMDTXT_OPTIONS_RANDOMSELLTEXT_OFF);
		else
			Watto_Options[Watto_Realm][Watto_Player]["randomselltext"] = "on";
			Watto_Msg(WATTO_CMDTXT_OPTIONS_RANDOMSELLTEXT_ON);
		end
	elseif cmd == WATTO_CMD_OPTIONS_RANDOMSELLTEXT_ON then
		Watto_Options[Watto_Realm][Watto_Player]["randomselltext"] = "on";
		Watto_Msg(WATTO_CMDTXT_OPTIONS_RANDOMSELLTEXT_ON);
	elseif cmd == WATTO_CMD_OPTIONS_RANDOMSELLTEXT_OFF then
		Watto_Options[Watto_Realm][Watto_Player]["randomselltext"] = "off";
		Watto_Msg(WATTO_CMDTXT_OPTIONS_RANDOMSELLTEXT_OFF);
	elseif cmd == WATTO_CMD_OPTIONS then
		Watto_Msg(WATTO_HELP_OPTIONS);
	elseif cmd == WATTO_CMD_LIST_ME then
		local list = WATTO_LIST_PERSONALLISTING.."\r";
		local count = Watto_tcount(Watto_ItemList["PerChar"][Watto_Realm][Watto_Player]);

		if count > 0 then
			local c = 1;
			for k,v in pairs(Watto_ItemList["PerChar"][Watto_Realm][Watto_Player]) do
				local _,link = GetItemInfo(k);
				list = list..c..") "..link;
				if c < count then
					list = list.."\r";
				end
				c = c+1;
			end
		else
			list = WATTO_LIST_EMPTYPERSONAL;
		end
		Watto_Msg(list);
	elseif cmd == WATTO_CMD_LIST then
		local list = {};
		local lnum = 0;
		local countmax = 10;
		local count = Watto_tcount(Watto_ItemList["General"]);

		if count > 0 then
			local c = 1;
			local counter = 0;
			for k,v in pairs(Watto_ItemList["General"]) do
				local _,link = GetItemInfo(k);
				if (link) then
					if (not list[lnum]) then
						list[lnum] = "";
					end
					if (counter ~= 0) then
						list[lnum] = list[lnum].."\r";
					end
					list[lnum] = list[lnum]..c..") "..link;
					c = c+1;
					counter = counter+1;
					if (counter == countmax) then
						lnum = lnum+1;
						counter = 0;
					end
				end
			end
		else
			list[0] = WATTO_LIST_EMPTYGENERAL;
		end
		Watto_Msg(WATTO_LIST_GENERALLISTING);
		for x=0, lnum do
			Watto_Msg(list[x]);
		end
	elseif (cmd == "dev") then
		Watto_Msg("Dev-Test Function: Item Sell List");
		local baglist = Watto_ScanBags();
		local list, ilist = Watto_GetSellItems();
		for bag,v in pairs(list) do	
			for slot,d in pairs(list[bag]) do
				local id = tonumber(d);
				local _,itemlink = GetItemInfo(id);
				Watto_Msg(format("Bag: %d, Slot: %d, ID: %d, Item to Sell: %s ",bag,slot,id,itemlink));
			end
		end
		Watto_Msg("Test Function Complete");
	else
		Watto_CmdParse(cmd);
	end
end

local Commands = {
	WATTO_CMD_ADD_ME,
	WATTO_CMD_ADD,
	WATTO_CMD_REMOVE_ME,
	WATTO_CMD_REMOVE,
	WATTO_CMD_REM_ME,
	WATTO_CMD_REM,
}

function Watto_CmdParse(cmd)
	local listing, command = "","";
	local linkPat = "|?|h.-|h.-|h";
	local outmsg = "";

	-- Get the Command we're using
	for i=1, getn(Commands) do
		local a = Commands[i];

		if string.find(cmd,a) then
			local com = string.find(cmd,a);
			command = strtrim(strsub(cmd,com,com+strlen(a)));
			--Watto_Msg("Command: \""..command.."\"")
			listing = string.gsub(cmd,command,"");
			break;
		end
	end

	if command == nil then
		Watto_Msg(WATTO_ERROR_UNKNOWNCOMMAND);
	end

	-- Watto_Msg("Items: \""..strtrim(tostring(gsub(listing, "\124", "\124\124"))).."\"");

	-- Generate the Items Table
	local items = {};
	for v in string.gmatch(listing, linkPat) do
		table.insert(items,Watto_GetItemID(v));
	end

	-- Apply the Command to the items
	for x=1,getn(items) do
		local _,link,_,itemLevel,_,itemType,itemSubType = GetItemInfo(items[x])
		local itemid = tonumber(items[x]);
		--Watto_Msg("Item: "..tostring(itemid));

		if (command == WATTO_CMD_ADD_ME) then
			--Watto_Msg("Attempting to add to Personal List...");
			if Watto_CheckforBadItem(items[x]) then
				outmsg=format(WATTO_ERROR_LIST_BADITEM,link);
			elseif (not Watto_ItemList["PerChar"][Watto_Realm][Watto_Player][items[x]]) then
				local text_gen = WATTO_LIST_ADDPERSONALBUTINGENERAL_SUCCESS;
				local text_per = WATTO_LIST_ADDPERSONAL_SUCCESS;
				local nofoodsell = false;

				for x=1,getn(Watto_Food_NoAutosell) do
					if (tonumber(Watto_Food_NoAutosell[x]) == itemid) then
						nofoodsell = true;
						break;
					end
				end

				if (Watto_Options[Watto_Realm][Watto_Player]["autosellfood"] == "on") and (itemType == WATTO_ITEMTYPE_CONSUMABLE) and (itemSubType == WATTO_ITEMSUBTYPE_FOODANDDRINK) and (nofoodsell == false) and (itemLevel+5 < UnitLevel("Player")) then
					text_gen = WATTO_LIST_ADDPERSONALBUTINGENERALFOOD_SUCCESS;
					text_per = WATTO_LIST_ADDFOOD_SUCCESS;
				end

				Watto_ItemList["PerChar"][Watto_Realm][Watto_Player][items[x]] = 1;
				if (Watto_ItemList["General"][items[x]]) then
					outmsg=format(text_gen,link);
				else
					outmsg=format(text_per,link);
				end
			else
				outmsg=format(WATTO_LIST_ADDPERSONAL_FAIL,link);
			end
		elseif (command == WATTO_CMD_ADD) then
			local text = WATTO_LIST_ADDGENERAL_SUCCESS;
			local nofoodsell = false;
			local isfood = false;

			for x=1,getn(Watto_Food_NoAutosell) do
				if (tonumber(Watto_Food_NoAutosell[x]) == itemid) then
					nofoodsell = true;
					break;
				end
			end

			if (Watto_Options[Watto_Realm][Watto_Player]["autosellfood"] == "on") and (itemType == WATTO_ITEMTYPE_CONSUMABLE) and (itemSubType == WATTO_ITEMSUBTYPE_FOODANDDRINK) and (nofoodsell == false) and (itemLevel+5 < UnitLevel("Player")) then
				text = WATTO_LIST_ADDFOOD_SUCCESS;
			end

			--Watto_Msg("Attempting to add to Global List...");
			if Watto_CheckforBadItem(items[x]) then
				outmsg=format(WATTO_ERROR_LIST_BADITEM,link);
			elseif (not Watto_ItemList["General"][items[x]]) then
				Watto_ItemList["General"][items[x]] = 1;
				outmsg=format(text,link);
			else
				outmsg=format(WATTO_LIST_ADDGENERAL_FAIL,link);
			end
		elseif (command == WATTO_CMD_REM_ME) or (command == WATTO_CMD_REMOVE_ME) then
			if (Watto_ItemList["PerChar"][Watto_Realm][Watto_Player][items[x]]) then
				Watto_ItemList["PerChar"][Watto_Realm][Watto_Player] = Watto_RemoveFromTable(Watto_ItemList["PerChar"][Watto_Realm][Watto_Player],items[x]);
				outmsg=format(WATTO_LIST_REMPERSONAL_SUCCESS,link);
			else
				outmsg=format(WATTO_LIST_REMPERSONAL_FAIL,link);
			end
		elseif (command == WATTO_CMD_REM) or (command == WATTO_CMD_REMOVE) then
			if (Watto_ItemList["General"][items[x]]) then
				Watto_ItemList["General"] = Watto_RemoveFromTable(Watto_ItemList["General"],items[x]);
				outmsg=format(WATTO_LIST_REMGENERAL_SUCCESS,link);
			else
				outmsg=format(WATTO_LIST_REMGENERAL_FAIL,link);
			end
		end
		Watto_Msg(outmsg);
	end
end

function Watto_SellMoneyEstimate()
	local _,itemlist = Watto_GetSellItems();
	local money = 0;

	for k in pairs(itemlist) do
		local _,_,_,_,_,_,_,_,_,_, itemSellPrice = GetItemInfo(k);
		local count = GetItemCount(k);
		money = money+(itemSellPrice*count);
	end

	return money;
end

function Watto_SellJunk()
	if (not MerchantFrame:IsVisible()) then
		Watto_Msg("Merchant Window Not Opened.");
		return;
	end
	if (MerchantFrame.selectedTab ~= 1) then
		Watto_Msg("Not on Main Merchant Tab.");
		return;
	end

	local list, itemlist = Watto_GetSellItems();
	local money = Watto_SellMoneyEstimate();

	if (list == nil) then
		Watto_Msg(WATTO_ERROR_NOITEMS);
	else
		for bag,v in pairs(list) do	
			for slot,d in pairs(list[bag]) do
				Watto_Msg("Successfully got to the Sell Point for Bag"..bag..", Slot"..slot);
				UseContainerItem(bag, slot);
			end
		end
		for id in pairs(itemlist) do
			if (Watto_Options[Watto_Realm][Watto_Player]["sellnotify"] == "on") then
				local count = GetItemCount(id);
				local _,itemlink = GetItemInfo(id);
				local tc = "";
				if count > 1 then
					tc = "x"..count;
				end
				Watto_Msg(format(WATTO_SELL_ITEM,itemlink)..tc);
			end
		end
	end

	if (money > 0) then
		local outtxt = "";
		if Watto_Options[Watto_Realm][Watto_Player]["randomselltext"] == "on" then
			local rand = Watto_SetLen(random(1,WATTO_SELL_TOTALGAINSNUM),2);
			outtxt = format(getglobal("WATTO_SELL_TOTALGAINS"..rand), Watto_Color_02..Watto_CSG(money).."|r");
		else
			outtxt = format(WATTO_SELL_TOTALGAINS00, Watto_Color_02..Watto_CSG(money).."|r");
		end
		Watto_Msg(outtxt);
	end
end

function Watto_Tooltip_Sell(frame)
	local money = Watto_SellMoneyEstimate();

	local text = format(WATTO_TOOLTIP_SELL_TEXT,Watto_CSG(money));

	WattoTooltip:SetOwner(frame, "ANCHOR_CURSOR");
	WattoTooltip:SetText(WATTO_TOOLTIP_SELL_TITLE);
	WattoTooltip:AddLine(text,1,1,1);
	WattoTooltip:Show();
end

function Watto_Merchant_ChangeTab()
	orig_MerchantFrame_Update()
	if (MerchantFrame.selectedTab == 1) then
		Watto_SellJunkButton:Show();
	else
		Watto_SellJunkButton:Hide();
	end
end

function Watto_Merchant_OnShow()
	if (MerchantFrame.selectedTab == 1) then
		if (Watto_Options[Watto_Realm][Watto_Player]["autosell"]) and (Watto_Options[Watto_Realm][Watto_Player]["autosell"] == "on") then
			Watto_SellJunk();
		end
	end
end

function Watto_Buy_OnModClick(frame,button,...)
	if (HandleModifiedItemClick(GetMerchantItemLink(frame:GetID()))) then
		return;
	end	
	if (MerchantFrame.selectedTab == 1) then
		Watto_Buy_CurrentButton = frame;
		Watto_Buy_CurrentItem = frame:GetID();
		local ItemName = GetMerchantItemInfo(Watto_Buy_CurrentItem);
		local _, _, _, quantity = GetMerchantItemInfo(Watto_Buy_CurrentItem);

		-- Set Text
		Watto_Buy_Title:SetText(WATTO_BUY_TITLE);
		Watto_Buy_FrameButton_1:SetText(WATTO_BUY_STACK);
		Watto_Buy_FrameButton_2:SetText(WATTO_BUY_CANAFFORD);
		Watto_Buy_FrameButton_3:SetText(WATTO_BUY_FILLSTACKS);
		Watto_Buy_FrameButton_4:SetText(WATTO_BUY_FILLBAGS);
		Watto_Buy_ItemName:SetText(ItemName);

		Watto_Buy_FrameButton_Purchase:SetText(WATTO_BUY_PURCHASE);

		Watto_Buy_Cost_Title:SetText(WATTO_BUY_COST);
		Watto_Buy_Count:SetText(quantity);
		Watto_Buy_OnChange();
		Watto_Buy_FirstCall = true;

		Watto_Buy_Frame:ClearAllPoints();
		Watto_Buy_Frame:SetPoint("TOPLEFT", MerchantFrame, "TOPRIGHT", -39, -7);
		Watto_Buy_Frame:Show();
		Watto_Buy_Count:SetFocus();
	else
		local vars = {}
		for i=0,arg.n do
			var[i] = arg[i]
		end
		Orig_MerchantItemButton_OnModifiedClick(frame,button,vars);
	end
end

function Watto_Buy_ButtonOnClick(self)
	local name = self:GetName();
	-- Watto_Msg(name.." was pressed.");

	if name == "Watto_Buy_FrameButton_Purchase" then
		Watto_Buy_PrePurchase();
	elseif name == "Watto_Buy_FrameButton_1" then
		Watto_Buy_Button_AddStack();
	elseif name == "Watto_Buy_FrameButton_2" then
		Watto_Buy_Button_CanAfford();
	elseif name == "Watto_Buy_FrameButton_3" then
		Watto_Buy_Button_FillStacks();
	elseif name == "Watto_Buy_FrameButton_4" then
		Watto_Buy_Button_FillBags();
	end
end

function Watto_Buy_Button_AddStack()
	local itemname, texture, price, stack, numAvailable, isUsable, extendedCost = GetMerchantItemInfo(Watto_Buy_CurrentItem);
	local itemlink = GetMerchantItemLink(Watto_Buy_CurrentItem);
	local _, _, _, _, _, _, _, itemStackCount = GetItemInfo(itemlink);
	local count = Watto_Buy_Count:GetNumber();
	if (not count) or (count < stack) or (count == stack) then
		count = 0;
	end

	local lcount = count + itemStackCount;
	if (numAvailable > 0) and (numAvailable < lcount) then
		lcount = numAvailable;
	end

	Watto_Buy_FirstCall = false;
	Watto_Buy_Count:SetText(lcount);
	Watto_Buy_OnChange();
end

function Watto_Buy_Button_CanAfford()
	local _, _, price, stack, numAvailable = GetMerchantItemInfo(Watto_Buy_CurrentItem);
	local mymoney = GetMoney();
	local cost_honor,cost_arena,cost_itemcount = GetMerchantItemCostInfo(Watto_Buy_CurrentItem);
	local honor, arena = GetHonorCurrency(), GetArenaCurrency();
	local newcount = 0;
	local items = {};

	local costperquanity = {
		["money"] = price,
		["honor"] = cost_honor,
		["arena"] = cost_arena,
		["items"] = {},
	}
	if cost_itemcount > 0 then
		for i=1,cost_itemcount do
			local _, ival, ilink = GetMerchantItemCostItem(Watto_Buy_CurrentItem,i);
			items[ilink] = GetItemCount(ilink);
			costperquanity["items"][ilink] = ival;
		end
	end

	local notmax = false;
	while notmax == false do
		local tripped = false;
		if (mymoney < costperquanity["money"]) then
			tripped = true;
		end
		if (honor < costperquanity["honor"]) then
			tripped = true;
		end
		if (arena < costperquanity["arena"]) then
			tripped = true;
		end
		for link,num in pairs(costperquanity["items"]) do
			if (items[link] < num) then
				tripped = true;
			end
		end

		if (tripped == true) then
			notmax = true;
		else
			newcount = newcount + 1;
			mymoney = mymoney - costperquanity["money"];
			honor = honor - costperquanity["honor"];
			arena = arena - costperquanity["arena"];
			for link,v in pairs(items) do
				items[link] = items[link] - costperquanity["items"][link];
			end
		end
	end

	if stack > 1 then
		newcount  = newcount * stack;
	end

	if (numAvailable > 0) and (numAvailable < newcount) then
		newcount = numAvailable;
	end

	-- newcount = floor(floor(mymoney/(price/stack))/stack)*stack;

	Watto_Buy_FirstCall = false;
	Watto_Buy_Count:SetNumber(newcount);
	Watto_Buy_OnChange();
end

function Watto_Buy_Button_FillStacks()
	local _, _, price, quantity, numAvailable = GetMerchantItemInfo(Watto_Buy_CurrentItem);
	local cid = Watto_GetItemID(GetMerchantItemLink(Watto_Buy_CurrentItem));
	local _, _, _, _, _, _, _, itemStackCount = GetItemInfo(cid);
	local bagdata = Watto_ScanBags();
	local count = 0;

	for Bag,v in pairs(bagdata) do
		for Slot,v in pairs(bagdata[Bag]) do
			for itemid,icount in pairs(bagdata[Bag][Slot]) do
				if (itemid == cid) then
					local ccount = itemStackCount-icount;
					count = count + ccount;
				end
			end
		end
	end

	if count == 0 then
		count = itemStackCount;
	end
	if (numAvailable > 0) and (numAvailable < count) then
		count = numAvailable;
	end

	count = floor(count/quantity)*quantity;

	Watto_Buy_FirstCall = false;
	Watto_Buy_Count:SetNumber(count);
	Watto_Buy_OnChange();
end

function Watto_Buy_Button_FillBags()
	local _, _, _, quantity, numAvailable = GetMerchantItemInfo(Watto_Buy_CurrentItem);
	local cid = Watto_GetItemID(GetMerchantItemLink(Watto_Buy_CurrentItem));
	local _, _, _, _, _, _, _, itemStackCount = GetItemInfo(cid);
	local totalslots = 0;
	local count = 0;

	for Bag=0,NUM_BAG_SLOTS do
		local slots, type = GetContainerNumFreeSlots(Bag);
		if (type == "bag") then
			totalslots = totalslots + slots;
		end
	end

	local ic = totalslots*itemStackCount;
	if (numAvailable > 0) and (numAvailable < ic) then
		ic = numAvailable;
	end

	count = floor(ic/quantity)*quantity;

	Watto_Buy_FirstCall = false;
	Watto_Buy_Count:SetNumber(ic);
	Watto_Buy_OnChange();
end

function Watto_Buy_PrePurchase()
	local count = Watto_Buy_Count:GetNumber();
	local iname, _, price, quantity = GetMerchantItemInfo(Watto_Buy_CurrentItem);
	local honorPoints, arenaPoints, itemCount = GetMerchantItemCostInfo(Watto_Buy_CurrentItem);
	local cid = Watto_GetItemID(GetMerchantItemLink(Watto_Buy_CurrentItem));
	local _, _, _, _, _, _, _, itemStackCount = GetItemInfo(cid);
	local maxslots = 0;
	local mymoney = GetMoney();
	local maxafford = floor(floor(mymoney/(price/quantity))/quantity)*quantity;
	local bagdata, freeslots = Watto_ScanBags();

	maxslots = freeslots * ((itemStackCount/quantity)*quantity);

	if (count > maxafford) then
		Watto_Msg(WATTO_BUY_ERROR_TOOEXPENSIVE,"error");
		return;
	end
	if (count > maxslots) then
		Watto_Msg(WATTO_BUY_ERROR_TOOMANY,"error");
		return;
	end

	if ((honorPoints ~= 0) or (arenaPoints ~= 0) or (itemCount ~= 0) ) then
		Watto_ConfirmCurrencyPurchase(count);
	else
		Watto_Purchase(count);
	end
end

function Watto_ConfirmCurrencyPurchase(count)
	local iname, _, price, quantity = GetMerchantItemInfo(Watto_Buy_CurrentItem);
	local honorPoints, arenaPoints, itemCount = GetMerchantItemCostInfo(Watto_Buy_CurrentItem);
	local itemTexture, itemLink, itemsString, pointsTexture;

	honorPoints, arenaPoints, itemCount = (honorPoints or 0) * count, (arenaPoints or 0) * count, (itemCount or 0) * count;
	
	if ( honorPoints and honorPoints ~= 0 ) then
		local factionGroup = UnitFactionGroup("player");
		if ( factionGroup ) then	
			pointsTexture = "Interface\\PVPFrame\\PVP-Currency-"..factionGroup;
			itemsString = " |T" .. pointsTexture .. ":0:0:0:-1|t" ..  honorPoints .. " " .. HONOR_POINTS;
		end
	end
	if ( arenaPoints and arenaPoints ~= 0 ) then
		if ( itemsString ) then
			-- adding an extra space here because it looks nicer
			itemsString = itemsString .. "  |TInterface\\PVPFrame\\PVP-ArenaPoints-Icon:0:0:0:-1|t" .. arenaPoints .. " " .. ARENA_POINTS;
		else
			itemsString = " |TInterface\\PVPFrame\\PVP-ArenaPoints-Icon:0:0:0:-1|t" .. arenaPoints .. " " .. ARENA_POINTS;
		end
	end
	
	local maxQuality = 0;
	for i=1, MAX_ITEM_COST, 1 do
		itemTexture, itemCount, itemLink = GetMerchantItemCostItem(Watto_Buy_CurrentItem, i);
		if ( itemLink ) then
			local _, _, itemQuality = GetItemInfo(itemLink);
			maxQuality = math.max(itemQuality, maxQuality);
			if ( itemsString ) then
				itemsString = itemsString .. LIST_DELIMITER .. format(ITEM_QUANTITY_TEMPLATE, (itemCount or 0) * count, itemLink);
			else
				itemsString = format(ITEM_QUANTITY_TEMPLATE, (itemCount or 0) * count, itemLink);
			end
		end
	end

	if ( honorPoints == 0 and arenaPoints == 0 and maxQuality <= ITEM_QUALITY_UNCOMMON ) then
		Watto_Purchase(count);
		return;
	end

	local itemName, _, itemQuality = GetItemInfo(Watto_Buy_CurrentButton.link);
	local r, g, b = GetItemQualityColor(itemQuality);
	Watto_Count = count;
	StaticPopup_Show("WATTO_CONFIRMPURCHASETOKENITEM", itemsString, "", {["texture"] = Watto_Buy_CurrentButton.texture, ["name"] = itemName, ["color"] = {r, g, b, 1}, ["link"] = Watto_Buy_CurrentButton.link, ["index"] = Watto_Buy_CurrentItem, ["count"] = count});
end

function Watto_Purchase(count)
	local iname, _, _, quantity = GetMerchantItemInfo(Watto_Buy_CurrentItem);
	local cid = Watto_GetItemID(GetMerchantItemLink(Watto_Buy_CurrentItem));
	local _, _, _, _, _, _, _, itemStackCount = GetItemInfo(cid);
	local bagdata = Watto_ScanBags();
	local existingstacks = {};
	local debug = 0; -- Flip to 1 to switch from buying to text output.

	local ecount = 0;
	for Bag,v in pairs(bagdata) do
		for Slot,v in pairs(bagdata[Bag]) do
			for itemid,icount in pairs(bagdata[Bag][Slot]) do
				if (itemid == cid) then
					ecount = ecount + 1;
					existingstacks[ecount] = {
						["Bag"] = Bag,
						["Slot"] = Slot,
						["Count"] = icount,
					}
				end
			end
		end
	end

	-- If bought in amounts greater than 1
	if (quantity > 1) then
		-- If buying a stack or more...
		while count >= itemStackCount do
			local amount = floor(itemStackCount/quantity);
			if debug == 0 then BuyMerchantItem(Watto_Buy_CurrentItem,amount); else Watto_Msg("Buying a Q stack ("..itemStackCount..") of "..iname); end
			count = count-itemStackCount;
		end
		-- Purchase leftover amount
		if (count > 0) and (count < itemStackCount) then
			local amount = floor(count/quantity);
			if debug == 0 then BuyMerchantItem(Watto_Buy_CurrentItem,amount); else Watto_Msg("Buying Q "..iname.." x"..(amount*quantity)); end
		end
	else
		-- Buy Full Stacks
		while count >= itemStackCount do
			local amount = itemStackCount;

			if debug == 0 then BuyMerchantItem(Watto_Buy_CurrentItem,amount); else Watto_Msg("Buying a stack ("..itemStackCount..") of "..iname); end
			count = count-itemStackCount;
		end
		-- Fill Existing Stacks
		if (count > 0) and (getn(existingstacks) > 0) then
			for i=1,getn(existingstacks) do
				local amount = itemStackCount-existingstacks[i]["Count"];
				if (count >= amount) then
					if debug == 0 then BuyMerchantItem(Watto_Buy_CurrentItem,amount); else Watto_Msg("Filling existing stacks of "..iname.." x"..amount); end
					count = count-amount;	
				end
			end
		end
		-- Buy Remaining Amount
		if (count > 0) then
			if debug == 0 then BuyMerchantItem(Watto_Buy_CurrentItem,count); else Watto_Msg("Buying "..iname.." x"..count); end
		end
	end

	Watto_Merchant_OnHide();
end

function Watto_Buy_OnChar(newnum)
	local num = tonumber(newnum);
	local type = type(num);
	--Watto_Msg("Num="..tostring(num)..", type="..tostring(type));
	if (Watto_Buy_FirstCall == true) and (type == "number") and (num > 0) then
		--Watto_Msg("FirstCall!")
		Watto_Buy_Count:SetNumber(tonumber(newnum));
		Watto_Buy_OnChange();
		Watto_Buy_FirstCall = false;
	end
end

function Watto_Buy_OnChange()
	local count = Watto_Buy_Count:GetNumber();
	local newcount, maxslots, totalslots = 0, 0, 0;
	local _, _, price, quantity = GetMerchantItemInfo(Watto_Buy_CurrentItem);
	local cid = Watto_GetItemID(GetMerchantItemLink(Watto_Buy_CurrentItem));
	local _, _, _, _, _, _, _, itemStackCount = GetItemInfo(cid);
	local cost = (price*(count/quantity));
	local mymoney = GetMoney();
	local countfont = Watto_Count_Font;
	local costcolor = HIGHLIGHT_FONT_COLOR_CODE;
	local cost_honor,cost_arena,cost_itemcount = GetMerchantItemCostInfo(Watto_Buy_CurrentItem);
	local finalcost = "";

	for Bag=0,NUM_BAG_SLOTS do
		local slots, type = GetContainerNumFreeSlots(Bag);
		if (type == 0) then
			totalslots = totalslots + slots;
		end
	end
	maxslots = totalslots * ((itemStackCount/quantity)*quantity);

	--Watto_Msg(count.." VS "..maxslots);
	if (count > maxslots) then
		countfont = Watto_Count_Font_Red;
	end

	--Watto_Msg("Honor:"..cost_honor..", Arena:"..cost_arena..", ItemNum:"..cost_itemcount)

	if (cost > 0) then
		if (cost > mymoney) then
			costcolor = RED_FONT_COLOR_CODE;
		end
		finalcost = finalcost..costcolor..Watto_CSGT(cost).."|r";
		costcolor = HIGHLIGHT_FONT_COLOR_CODE;
	end
	if (cost_honor > 0) then
		local c_honor = GetHonorCurrency();
		local faction = UnitFactionGroup("player");
		local c_cost = (cost_honor/quantity)*count;
		local currency = "Interface\\PVPFrame\\PVP-Currency-"..faction;
		if (c_cost > c_honor) then costcolor = RED_FONT_COLOR_CODE; else costcolor = HIGHLIGHT_FONT_COLOR_CODE;	end
		finalcost = finalcost..costcolor..c_cost.."\124T"..currency..":20:20:0:-2\124t";
	end
	if (cost_arena > 0) then
		local c_arena = GetArenaCurrency();
		local c_cost = (cost_arena/quantity)*count;
		if (c_cost > c_arena) then costcolor = RED_FONT_COLOR_CODE; else costcolor = HIGHLIGHT_FONT_COLOR_CODE;	end
		finalcost = finalcost..costcolor..c_cost.."|TInterface\\PVPFrame\\PVP-ArenaPoints-Icon:20:20:0:-2|t";
	end
	if (cost_itemcount > 0) then
		for i=1,cost_itemcount do
			local itex, ival, ilink = GetMerchantItemCostItem(Watto_Buy_CurrentItem,i);
			local i_count = GetItemCount(ilink);
			local c_cost = (ival/quantity)*count;
			if (c_cost > i_count) then costcolor = RED_FONT_COLOR_CODE; else costcolor = HIGHLIGHT_FONT_COLOR_CODE;	end
			finalcost = finalcost..costcolor..c_cost.."\124T"..itex..":18:18:0:-1\124t";
		end
	end

	Watto_Buy_Count:SetFontObject(countfont);
	Watto_Buy_Cost_Money:SetText(finalcost);
end

function Watto_Merchant_OnHide()
	orig_CloseMerchant();
	Watto_Buy_Frame:Hide();
	PlaySound("igCharacterInfoClose");
end

function Watto_TranslateMoney(money)
	if money == nil then
		money = "0c";
	else
		money = tostring(money);
	end
	local c, s, g = nil;
	
	c = strsub(money,-2);
	if (strlen(money)>2) then
		s = strsub(money,-4,-3)
	end
	if (strlen(money)>4) then
		g = strsub(money,0,strlen(money)-4)
	end
	return c, s, g;
end

function Watto_CSG(money)
	local c, s, g = Watto_TranslateMoney(money);
	local text = "";
	if (g ~= nil) then
		text = g..WATTO_GOLD_S;
	end
	if (s ~= nil) and (s ~= "00") then
		text = text..s..WATTO_SILVER_S;
	end
	if (c ~= nil) and (c ~= "00") then
		text = text..c..WATTO_COPPER_S;
	end
	return text;
end

function Watto_CSGT(money)
	local c, s, g = Watto_TranslateMoney(money);
	local text = "";
	if (g ~= nil) then
		text = g.."|TInterface\\MoneyFrame\\UI-GoldIcon:16:16:0:-1|t";
	end
	if (s ~= nil) and (s ~= "00") then
		text = text..s.."|TInterface\\MoneyFrame\\UI-SilverIcon:16:16:0:-1|t";
	end
	if (c ~= nil) and (c ~= "00") then
		text = text..c.."|TInterface\\MoneyFrame\\UI-CopperIcon:16:16:0:-1|t";
	end
	return text;
end

function Watto_GetItemID(itemlink)
	if not itemlink then return end
	if (strfind(itemlink,":") == nil) then return end
	--DEFAULT_CHAT_FRAME:AddMessage(format("get ID inLink: %s",tostring(itemlink)))
	local justItemId = string.gsub(itemlink,".-\124H([^\124]*)\124h.*", "%1");
	--DEFAULT_CHAT_FRAME:AddMessage(format("justid: %s",tostring(justItemId)))
	local split = strsplit(":",justItemId);
	local itype, itemid = split[1], split[2]
	--DEFAULT_CHAT_FRAME:AddMessage(format("itemtype: %s, itemid: %s",tostring(itype), tostring(itemid)))
	
	return itemid
end

-- tcount: count table members even if they're not indexed by numbers
function Watto_tcount(tab)
   local n = getn(tab)
   if (n == 0) then
     for _ in pairs(tab) do
       n = n + 1
     end
   end
   return n
end

function Watto_RemoveFromTable(table,index)
	local temp = {}

	for k,v in pairs(table) do
		if k ~= index then
			temp[k] = v;
		end
	end
	return temp;
end

function Watto_SetLen(value,length)
	local l = strlen(value);
	while l < length do
		value = "0"..value;
		l = strlen(value);
	end

	return value;
end

-- Bad Item Check
-- Returns nil if okay, returns true if a bad item is found.
function Watto_CheckforBadItem(itemid, bag, slot)

	-- Check Sell Price
	local iname, _, _, _, _, _, _, _, _, _, itemSellPrice = GetItemInfo(itemid);
	if itemSellPrice == nil or itemSellPrice == 0 then
		-- Watto_Msg(iname.."'s sell price is "..tostring(itemSellPrice))
		return true;
	end

--[[

Soulbound items not checked. Might add this back in in the future.

	-- Check for Soulbound.
	local tooltip = GameTooltip;
	if bag ~= nil and slot ~=nil then
		tooltip:SetBagItem(bag,slot)
	else
		local _,link = GetItemInfo(itemid);
		tooltip:SetHyperlink(link)
	end
	if Watto_TooltipFind(tooltip,ITEM_SOULBOUND) or Watto_TooltipFind(tooltip,ITEM_BIND_ON_EQUIP) or Watto_TooltipFind(tooltip,ITEM_BIND_ON_PICKUP) or Watto_TooltipFind(tooltip,ITEM_BIND_ON_USE) or Watto_TooltipFind(tooltip,ITEM_BIND_QUEST) or Watto_TooltipFind(tooltip,ITEM_BIND_TO_ACCOUNT) then
		return true;
	end

]]

	return;
end

-- Tooltip Searching
function Watto_TooltipFind(self, target)
	for i=1,self:NumLines() do
		local mytextL = getglobal(self:GetName().."TextLeft"..i)
		local mytextR = getglobal(self:GetName().."TextRight"..i)
		local textL = mytextL:GetText()
		local textR = mytextR:GetText()
		if strfind(textL,target) then
			return true;
		end
		if strfind(textR,target) then
			return true;
		end
	end

	return;
end