--[[
	Watto's Math file
	v1.3
]]

local function GetContainerNumFreeSlots(bagID)
	local fs = 0
	local bagtype = 0
	
	local InventoryID = ContainerIDToInventoryID(bagID)
	--DEFAULT_CHAT_FRAME:AddMessage(format("InventoryID: %s",tostring(InventoryID)))
	local bagLink = GetInventoryItemLink("player",InventoryID)
	--DEFAULT_CHAT_FRAME:AddMessage(format("baglink: %s",tostring(bagLink)))
	if not bagLink then
		--DEFAULT_CHAT_FRAME:AddMessage("Baglink not found")
		return 
	end
	local iname,ilink,irare,minlvl,itype,isubtype,istack, ieqloc,itex = GetItemInfo(Watto_GetItemID(bagLink))
	--DEFAULT_CHAT_FRAME:AddMessage(format("name: %s, type:%s, subtype: %s",tostring(iname),tostring(itype),tostring(isubtype)))
	
	for slot=1, GetContainerNumSlots(bagID) do
		local item = GetContainerItemLink(bagID, slot)
		if not item then
			fs = fs + 1
		end
	end
	return fs, isubtype
end

function Watto_ScanBags()
	local bagdata = {};
	local freeslots = 0, 0;

	for Bag=0,NUM_BAG_SLOTS do
		if not bagdata[Bag] then
			bagdata[Bag] = {};
		end

		local NumSlots = GetContainerNumSlots(Bag);
		local slots, type = GetContainerNumFreeSlots(Bag);
		--Watto_Msg("bag subtype: "..tostring(type))
		if (type == "Bag") then
			freeslots = freeslots + slots;
		end

		for Slot=0,NumSlots do
			if not bagdata[Bag][Slot] then
				bagdata[Bag][Slot] = {}
			end
			local itemlink = GetContainerItemLink(Bag,Slot);
			if (itemlink ~= nil) then
				local ItemID = Watto_GetItemID(itemlink);
				if (not bagdata[Bag][Slot][ItemID]) then
					local _, icount = GetContainerItemInfo(Bag,Slot);
					bagdata[Bag][Slot][ItemID] = icount;
				end
			end
		end
	end

	return bagdata, freeslots;
end

function Watto_GetSellItems()
	local itemstosell, itemlist = {}, {};
	local GenList = Watto_ItemList["General"];
	local PlayList = Watto_ItemList["PerChar"][Watto_Realm][Watto_Player];
	local baglist = Watto_ScanBags();

	--for Bag,v in pairs(baglist) do
	for Bag=0,NUM_BAG_SLOTS do
		--for Slot,v in pairs(baglist[Bag]) do
		local NumSlots = GetContainerNumSlots(Bag);
		for Slot=1,NumSlots do
			--for itemid,v in pairs(baglist[Bag][Slot]) do
			local itemlink = GetContainerItemLink(Bag,Slot);
			if (itemlink ~= nil) then
				local itemid = tonumber(Watto_GetItemID(itemlink));
				local itemid_s = tostring(itemid);
				local itemname,itemlink,itemRarity,itemLevel,_,itemType,itemSubType,_,_,_ = GetItemInfo(itemid);
				local itemSellPrice = 0
				local sellitem = false;
				local isGen, isPriv;
				if (GenList[itemid_s]) then isGen = true; end
				if (PlayList)and(PlayList[itemid_s]) then isPriv = true; end
				
				-- Get the sell price
				if (MerchantWindow) then
					
				end

				if (Watto_TempSell[itemid_s]) and (Watto_TempSell[itemid_s] == 1) then
					sellitem = true;
					--Watto_Msg("Found TempSell Item "..itemlink.."! Attempting to Sell...")
				elseif (itemRarity == 0) then
					if ((isGen) and (isPriv)) then
						--Watto_Msg("Marked "..itemname.." for selling.");
						sellitem = true;
					elseif ((not isGen) and (not isPriv)) then
						--Watto_Msg("Marked "..itemname.." for selling.");
						sellitem = true;
					end
				elseif (itemRarity > 0) then
					if (Watto_Options["autosellfood"] == "on") 
						and ((itemType == WATTO_ITEMTYPE_CONSUMABLE) 
						and (itemSubType == WATTO_ITEMSUBTYPE_FOODANDDRINK) 
						and (itemLevel+5 < UnitLevel("Player"))) then
						--Watto_Msg("Processing Food Item: "..itemlink.."...");
						sellitem = true;
						for x=1,getn(Watto_Food_NoAutosell) do
							if tonumber(Watto_Food_NoAutosell[x]) == itemid then
								sellitem = false;
								break;
							end
						end
						if ((isGen) and (not isPriv)) then
							--Watto_Msg("Marked "..itemname.." for selling.");
							sellitem = false;
						elseif ((not isGen) and (isPriv)) then
							--Watto_Msg("Marked "..itemname.." for selling.");
							sellitem = false;
						end
					elseif ((isGen) and (not isPriv)) then
						--Watto_Msg("Marked "..itemname.." for selling.");
						sellitem = true;
					elseif ((not isGen) and (isPriv)) then
						--Watto_Msg("Marked "..itemname.." for selling.");
						sellitem = true;
					end
					--Watto_Msg(format("Test complete for %s. IsGen: %s, IsPriv: %s, Final Result: %s",itemlink,tostring(isGen),tostring(isPriv),tostring(sellitem)));
				end
				if (itemSellPrice < 1) then
					--Watto_Msg(format("Price of %s is less than 1. Not Selling.",itemlink));
					sellitem = false;
				end

				-- If we have to sell this...
				if (sellitem == true) then
					if (not itemstosell[Bag]) then
						itemstosell[Bag] = {};
					end
					if (not itemstosell[Bag][Slot]) then
						itemstosell[Bag][Slot] = itemid;
						if (not itemlist[itemid]) then
							--Watto_Msg("Adding "..itemname.." to the item sell list.");
							itemlist[itemid] = 1;
						end
					end
				end
			end
		end
	end

	return itemstosell, itemlist;
end

-- Useful Math Functions

function Watto_RoundDown(num, idp)
	local mult = 10^(idp or 0)
	return math.floor(num * mult) / mult
end
