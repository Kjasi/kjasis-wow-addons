Watto v1.3
----------------------------------------

Watto is a useful tool for buying massive amounts of items, and selling your junk items!

Commandline Help:

	add (Item) = This will add an item to your global sell list.
	add me (Item) = This adds an item to the current character's sell list.
	rem (Item) or remove (Item) = Remove an item from the global sell list.
	rem me (Item) or remove me (Item) = Remove an item from your personal sell list.
	rem all or remove all = Remove every item from the global sell list.
	rem me all or remove me all = Remove every item from your personal sell list.
	list = See a list of items on the general exclusion list.
	list me = See a list of items on your personal exclusion list.
	options autosell [on/off] = Toggle the autosell function on and off. You can also add "on" and "off" to specify a setting.
	options notify [on/off] = Toggle the listing of the items when you sell them.
	options randomtext [on/off] = Toggle Watto's random saying when you sell items to a vendor.
	autosell [on/off] = Toggle the automatic selling of your junk items.
	autosell food [on/off] = Toggle if Watto will automatically sell food and drink items.

----------------------------------------
F.A.Q.
----------------------------------------

Q: How do I add an item to sell?
A1: To add an item to the global sell list (the list that all your toons use) type "/watto add" then link the item(s).
A2: Adding an item to your current toon's private sell list, type "/watto add me" then link the item(s).

Q: Can I add more than 1 item at a time?
A: You can add as many items as you can link at once.

Q: I don't understand! What's an exception list? General list? Personal list? What??
A: Let's say you go out and kill a lot of things, but you get a bunch of items you don't want. Most of it is trash, but there are some white-quality items that you want to sell too. The Exception list lets you add those items to the group of items that will be sold to vendors when you hit the "Watto's Junkyard" button on the top-right hand side of a merchant window. In the case of grey-quality (junk) items, the exception list will make it so that item is NOT sold to the vendor. There are 2 types of lists: The General Exception list works for all toons, on all the servers. This is generally used for white-quality items such as various food and water items that you don't need. The Personal Exception list works the same, but is for the current toon only. If you need the food and water items on all your toons, but don't need them on your mage, you can add them to his personal exception list, and any food or water on that toon will be sold, as if it was a junk item.

Q: I added a white-quality item to both my lists but it won't sell! How do I fix it?
A: When a white-quality item is added to the general sell list, it is considered a "trash" item. However, if that item is also on your current toon's private list, it is no longer considered a trash item. Type "/watto list me" and see if that item is in your private exception list. If it is, you can remove it by typing "/watto rem", link the item, and it will become sellable again.

Q: Watto keeps selling my food! Why is it doing that, and how do I stop it?!
A: Watto will automatically sell your food if it's not of a high enough level. There is usually better food you can use, so it sells your current food. To totally disable selling food, type "/watto autosell food off" into the chatbar. However, you can also add your specific food to either exclusion list, and they will no longer be automatically sold.

Q: How do I buy a lot of items?
A: When at a merchant, press the SHIFT key to open Watto's Discount Items window. There, you can type in the total number of items you want then press the "purchase" button, and Watto will buy them for you. Or you can press one of the handy buttons for Watto to automatically choose an amount for you!

----------------------------------------
History

v1.3
 - Watto will now automatically sell unhelpful Food and Drink items. This can be globally disabled by typing "/watto autosell food off" into any chat frame. Food listed in either exclusion list will not be sold. Certian items, like Deviate Fish, Savory Deviant Delight and Darkmoon Special Reserve will not be counted as Food & Drink items.
 - Added a Remove All function for both the General List and the Personal Lists.
 - The general exclusion List will now display in several short messages, rather than one huge message.
 - Watto will no longer attempt to sell unsellable items.
 - Fixed a bug when buying pre-stacked items where the purchase function didn't think you had enough money to buy the items.
 - Fixed a bug when viewing the general list, if an item didn't have a link for some reason.

v1.2.2
 - Update for Patch 3.3.5!

v1.2.1
 - Bugfix for a localization error I made, that would appear if saying 14 was triggered.

v1.2
 - Patched up for 3.3!
 - Watto now has a Temp Sell list, which can be used by other addons to add items to be automatically sold by Watto. Items in the Temp Sell list will not survive a reload. Quest Reward v1.2.5 (Also by Kjasi) is the first addon to make use of this function.
 - All the Watto's Bulk Supplies buttons will now respect limited-quanity item amounts.
 - Added 5 more random sayings for the English Localization.

v1.1
 - Watto's Bulk Supplies cost indicator now supports special cost types. (Honor Points, JC Tokens, Inks, Cooking Awards, Champion's Seals, ect.)
 - You can now just type in the number of items you want to buy. The default amount will be erased upon typing in an amount. Note: This function doesn't activate after pressing a button.
 - Updated the buttons to support non-money item costs.
 - Fixed a bug that would sometimes buy an additional item when filling stacks.
 - Fixed a bug that caused items sold in amounts greater than 1 to show the incorrect cost.
 - Fixed some Math in various functions.
 - Both the Junkyard Tooltip and the Bulk Supplies window now shows the money icons.
 - Fixed a localization issue where two items had the same name.


v1.0
 - Initial Release


----------------------------------------

Watto and his likeness are copyrighted to Lucasfilms. I mean only to honor this wonderful character by his usage.