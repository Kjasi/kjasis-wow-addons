# Kjasi's Addons

This is the home of Kjasi's addons for World of Warcraft!

## Appearances
A simple addon to help you figure out which appearances you have yet to unlock on your other characters!

## Watto
Watto is a bulk seller and buyer of items. It includes 1-button selling, which includes several smart options, such as selling BoP gear that you can't wear. He also modifies the bulk purchasing to allow for volumes greater than what WoW will natively let you purchase. It also includes buttons for filling stacks of items in your bags or reagent bank!

## QuestReward
Automatically selects the best-paying item from quests.

## RecipeKnown
A great addon that colors the recipes you know or don't know in the auction house or at merchants, to quickly tell you which you have, or if an alt needs that recipe! Includes a great tooltip that even tells you who can open a locked chest!

## MiniMount
A great addon that will look at your current level and skills and choose a random mount for you! How is this different from the random mount button in WoW? For starters, MiniMount will exclude flying mounts from the rotation when you can only use land mounts. It also offers you your sea turtle when you're swimming. Includes an updating macro so you know which mount you'll get next!

## Detached Mini Buttons
Detached Mini Buttons will let you arbitrarily move your Minimap buttons anywhere you want on the screen. This is an old favorite of mine that has long since been forgotten about. I've updated this addon and look towards improving it in the future!

## TitanSkills
A Titan Plugin that lets you quickly look up your profession skills!

## ItemID
This is more of a developer addon, but it easily lets you find all sort of data about items, quests achievements and spells. It also lets you find a link for an ID number!

# Currently in Beta

## Looter
Looter is a simple addon that tracks loot data of mobs, and applies that data to the tooltips of both mobs and items. It's great for figuring out which mobs drop the most Frostweave cloth!

# Discontinued

## BarMath
Barmath was an addon that replaces the XP and Reputation bars to give you a vast amount of data very quickly and easily. Cancelled due to several long-standing issues.