# Addon-Specific Information
$program = 'Watto';		# Addon Directory Name
$subpath = "";			# Subdirectory to use. Useful for when using a Subversion software with subdirectories.
$rev = "1.7.4";			# Addon Release Version. Replaces all instances of <%version%> in the Retail directory
$revClassic = "1.7.3";	# Classic Addon Release Version. Replaces all instances of <%version%> in the Classic directory