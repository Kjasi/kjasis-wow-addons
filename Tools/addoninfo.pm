# Global Information
$drive = 'E:/';		# The drive for the repositories & build path
$cdir = 'WoW/MyAddons/Addons/';		# Master Repository Directory with all the addons to compile
$retaildir = "08_BfA";		# Subdirectory of $cdir that contains your retail version of this addon.
$classicdir = "Classic";		# Subdirectory of $cdir that contains your Classic version of this addon.
$builddir = '~builds';		# Directory relative to $cdir in which to build the addons into.
$zipdir = '~zips';			# Directory relative to $cdir in which to put compressed archive files.
$compress_7z = "7z";		# 7-Zip Command-line program. Also used for exporting .zip files.
$compress_rar = "rar";		# RAR Command-line program.
$fnformat = "%s_v%s_Retail_80200";		# Archive Filename Format. Passed Vars: $program, $rev
$fnformatClassic = "%s_v%s_Classic_11302";		# Archive Filename Format for Classic releases. Passed Vars: $program, $rev
$vrchar = "\.";			# Version Replacement Character. Examples: If $rev is "1.3.4", using "_", $rev will become 1_3_4, "" = 134, and "\." = 1.3.4.
$make7z = 1;			# 1 will create a file with this compression type, 0 will not.
$makeZip = 1;			# If a file exists with the same addon name & version, it will be deleted.
$makeRar = 0;			# Make sure to update your $rev before running this script!

# Addon-Specific Information
$addonName = 'Watto';		# Addon Name