@echo off
@rem Builder Batch File
@rem Revision 1

@rem This is a batch file that will run our Perl script in Windows with a double-click. Also, it can be ran from the command-line anywhere on the system, and it will run the perl script.

@rem NOTE: This batch file MUST be in the same directory as the builder.pl file!

perl "%~dp0builder.pl"
pause