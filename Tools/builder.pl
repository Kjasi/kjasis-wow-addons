#
#	Kjasi's Release System Perl Script
#	Revision: 13
#
#	This script will gather the addon specified from a local hard-drive, and replace <%version%> and <%date%> with the
#	version number specified in addoninfo.pm, and today's date. Once that is done, if you've specified command-line
#	programs for 7-Zip and Rar programs, and turned the $make7z, $makeZip, and/or $makeRar to 1 in addoninfo.pm, this
#	will compress your addons into the files and place them into the $zipdrive, in a folder with the Addon's name.
#
#	A future revision of this script may see a way of using SVN/Merc/Git to gather the addon, instead of using the
#	local versions.
#
#	To run this, you will need Perl on your system.
#	I designed this for a Windows system, but tried to make it as universal as I could. If you run into problems on a
#	non-Windows system, send me your fix, and I'll update the official script.
#

######################################################################################
#
#	How to prepare to run this file on Windows:
#
#	1) Download and install Strawberry Perl (5.20+) or ActivePerl
#	2) Open a command prompt, and type: cpan File::Copy::Recursive
#	3) Install 7z command-line edition. (If you are exporting 7z or Zip files)
#	4) Install WinRAR. (If you are exporting rar files)
#
######################################################################################

######################################################################################
#
#	How to use this file:
#
#	1) Copy build.pl, build.bat and addoninfo.pm into the Expansion parent directory. Example: D:\WowFiles\MyMods\06_WoD
#	2) Open addoninfo.pm and update the Global Information variables to fit your addon production. These will rarely change.
#	3) Change the Addon-Specific Information to fit your current addon.
#	4) Run the builder.pl script or the builder.bat batch file to build your addon!
#
######################################################################################

#!/usr/local/bin/perl
use Cwd;
use strict;
use DateTime;
use File::Path qw(remove_tree);
use File::Copy::Recursive qw(rcopy);
use lib './';
require "addoninfo.pm";
require "addoninfo.$main::addonName.pm";

# Generated Information
my $dt = DateTime->from_epoch(epoch => time());
my $mn = $dt->month_name;
my $md = $dt->day;
my $year = $dt->year;
my $ordinal = 'th';
if ($md =~ /(?<!1)1$/) {
    $ordinal = 'st';
} elsif ($md =~ /(?<!1)2$/) {
    $ordinal = 'nd';
} elsif ($md =~ /(?<!1)3$/) {
    $ordinal = 'rd';
}
if (length($md) < 2){
	$md = "0$md";
}

my $date = "$mn $md$ordinal, $year";	# Today's Date. Will replaces all instances of <%date%>.
my $dcdir = "$main::drive$main::cdir";
my $rdir = "$dcdir$main::retaildir/$main::program";
my $cldir = "$dcdir$main::classicdir/$main::program";
my $buildpath = "$dcdir$main::builddir/$main::program";
my $zippath = "$main::zipdir/$main::program";
my $buildPathRetail = "$buildpath/Retail/$main::rev/$main::program";
$buildPathRetail =~ s/\s/_/gi;
my $buildPathClassic = "$buildpath/Classic/$main::revClassic/$main::program";
$buildPathClassic =~ s/\s/_/gi;
my $fileZipPathRetail = "$dcdir$zippath/$main::retaildir/";
my $fileZipPathClassic = "$dcdir$zippath/$main::classicdir/";
my $fileZipPathCombined = "$dcdir$zippath/Combined/";
my $hasRetail = 0;
my $hasClassic = 0;
if (-e $rdir and -d $rdir) {
	$hasRetail = 1;
}if (-e $cldir and -d $cldir) {
	$hasClassic = 1;
}
if ($hasRetail == 1 and $main::rev == "") {
	$hasRetail = 0;
}
if ($hasClassic == 1 and $main::revClassic == "") {
	$hasClassic = 0;
}

# Clear the screen
system $^O eq 'MSWin32' ? 'cls' : 'clear';
print "Has Retail: $hasRetail, Has Classic: $hasClassic\n\n";

if ($hasRetail == 0 and $hasClassic == 0) {
	print "Nothing to build. Aborting...\n\n";
	exit;
}

if ($hasRetail == 1) {
	print "Building $main::program for Retail, Version $main::rev, BuildPath: $buildPathRetail\n";
}
if ($hasClassic == 1) {
	print "Building $main::program for Classic, Version $main::revClassic, BuildPath: $buildPathClassic\n";
}
print"\n";



# Change Directory, or build it if we can't.
chdir($main::drive) or die "Can't connect to the $main::drive drive: $!";
if ($hasRetail == 1) {
	if (!chdir("$buildPathRetail")){
		print "Couldn't find Retail build directory. Building...\n";
		my @bpa = split('/',"$buildPathRetail");
		for my $i (@bpa) {
			mkdir($i);
			chdir($i);
		}
	}
}

if ($hasClassic == 1) {
	if (!chdir("$buildPathClassic")){
		print "Couldn't find Classic build directory. Building...\n";
		my @bpa = split('/',"$buildPathClassic");
		for my $i (@bpa) {
			mkdir($i);
			chdir($i);
		}
	}
}

print "Copying the files...\n";
if ($hasRetail == 1) {
	rcopy("$rdir$main::subpath", "$buildPathRetail") or die "Couldn't copy the retail files: $!";
}
if ($hasClassic == 1) {
	rcopy("$cldir$main::subpath", "$buildPathClassic") or die "Couldn't copy the retail files: $!";
}

# Remove Versioning Software Files
my @Libs;
my @VerFiles;
if ($hasRetail == 1) {
	my @VerFilesR = glob("$buildPathRetail/{.hg*,.git*}");
	push @VerFiles, @VerFilesR;
	my @LibsR = glob("$buildPathRetail/{*,*/*,*/*/*,*/*/*/*,*/*/*/*/*,*/*/*/*/*/*,*/*/*/*/*/*/*}/{.hg,.git,*.psd}");
	push @Libs, @LibsR;
}
if ($hasClassic == 1) {
	my @VerFilesCl = glob("$buildPathClassic/{.hg*,.git*}");
	push @VerFiles, @VerFilesCl;
	my @LibsCl = glob("$buildPathClassic/{*,*/*,*/*/*,*/*/*/*,*/*/*/*/*,*/*/*/*/*/*,*/*/*/*/*/*/*}/{.hg,.git,*.psd}");
	push @Libs, @LibsCl;
}
if (scalar(keys @VerFiles) gt 0 || scalar(keys @Libs) gt 0){
	print "\nRemoving Versioning Files...\n";
}
if ($hasRetail == 1) {
	if (-d "$buildPathRetail/.hg"){
		remove_tree("$buildPathRetail/.hg",1) or die "Couldn't remove retail versioning data: $!";
	}
	if (-d "$buildPathRetail/.git"){
		remove_tree("$buildPathRetail/.git",1) or die "Couldn't remove retail versioning data: $!";
	}
}
if ($hasClassic == 1) {
	if (-d "$buildPathClassic/.hg"){
		remove_tree("$buildPathClassic/.hg",1) or die "Couldn't remove classic versioning data: $!";
	}
	if (-d "$buildPathClassic/.git"){
		remove_tree("$buildPathClassic/.git",1) or die "Couldn't remove classic versioning data: $!";
	}
}
for my $l (@Libs) {
	if ((-f "$l") or (-d "$l")){
		remove_tree("$l") or die "Couldn't remove versioning data: $!";
	}
}
@VerFiles = glob();
if ($hasRetail == 1) {
	my @VerFilesR = glob("$buildPathRetail/{.hg*,.git*}");		#Update, since removing the .hg and .git folders were listed when scanned above
	push @VerFiles, @VerFilesR;
	
}
if ($hasClassic == 1) {
	my @VerFilesCl = glob("$buildPathClassic/{.hg*,.git*}");
	push @VerFiles, @VerFilesCl;
}
for my $l (@VerFiles) {
	unlink($l) or die "Couldn't delete versioning file ($l): $!";
}

chdir("$buildpath") or die "Can't change path. $!";

print "\nRelabeling the files...\n";

# Search the text files, and replace our strings

if ($hasRetail == 1) {
	my @filesR = glob("\"$buildPathRetail\"{*,*/*,*/*/*,*/*/*/*,*/*/*/*/*}/*.{lua,xml,toc,txt}");
	my $fs = 0+@filesR;
	print("Number of Files to Filter: $fs\n");
	for my $f (@filesR) {
		print("Processing $f...\n");
		local @ARGV = $f;
		$^I = '.bak';
		while(<>){
			s/<%version%>/$main::rev/;	# Replace all instances of <%version%> with $main::rev
			s/<%date%>/$date/;		# Replace all instances of <%date%> with $date
			print;
		}
		unlink $f.$^I;
	}
}

if ($hasClassic == 1) {
	my @filesCl = glob("\"$buildPathClassic\"{*,*/*,*/*/*,*/*/*/*,*/*/*/*/*}/*.{lua,xml,toc,txt}");
	my $fs = 0+@filesCl;
	print("Number of Files to Filter: $fs\n");
	for my $f (@filesCl) {
		print("Processing $f...\n");
		local @ARGV = $f;
		$^I = '.bak';
		while(<>){
			s/<%version%>/$main::revClassic/;	# Replace all instances of <%version%> with $main::rev
			s/<%date%>/$date/;		# Replace all instances of <%date%> with $date
			print;
		}
		unlink $f.$^I;
	}
}

# Compression Data
my $p = "$buildpath/\.\./";
chdir("$p") or die "Can't change path. Archiving Failed: $!";
$p = getcwd;
my $r = $main::rev;
my $rC = $main::revClassic;
$r =~ s/\./$main::vrchar/gi;
$rC =~ s/\./$main::vrchar/gi;
my $filebase = sprintf($main::fnformat,$main::program,$r);
$filebase =~ s/\s/_/gi;
my $filebaseCl = sprintf($main::fnformatClassic,$main::program,$rC);
$filebaseCl =~ s/\s/_/gi;

if (($main::compress_7z ne "")or($main::compress_rar ne "")){
	print("\nCompressing and Archiving Files...\n");
	print("Archive Path: [$p]\n");
}

# Compression Processes
if ($main::compress_7z ne ""){
	#7-Zip
	if ($main::make7z == 1){
		if ($hasRetail == 1) {
			my $file_7z = "$filebase.7z";
			if (-e "\"$fileZipPathRetail$file_7z\""){
				unlink("\"$fileZipPathRetail$file_7z\"") or warn "\nCould not delete [\"$fileZipPathRetail$file_7z\"]: $!";
			}
			print("\n7z Archive Filename: [$file_7z]\n");
			system("$main::compress_7z a -r -t7z -mx9 \"$fileZipPathRetail$file_7z\" $buildPathRetail/../*.*");
		}
		if ($hasClassic == 1) {
			my $file_7z = "$filebaseCl.7z";
			if (-e "\"$fileZipPathClassic$file_7z\""){
				unlink("\"$fileZipPathClassic$file_7z\"") or warn "\nCould not delete [\"$fileZipPathClassic$file_7z\"]: $!";
			}
			print("\n7z Archive Filename: [$file_7z]\n");
			system("$main::compress_7z a -r -t7z -mx9 \"$fileZipPathClassic$file_7z\" $buildPathClassic/../*.*");
		}
	}
	
	#Zip
	if ($main::makeZip == 1){
		if ($hasRetail == 1) {
			my $file_Zip = "$filebase.zip";
			if (-e "\"$fileZipPathRetail$file_Zip\""){
				unlink("$$fileZipPathRetail$file_Zip") or warn "\nCould not delete [\"$fileZipPathRetail$file_Zip\"]: $!";
			}
			print("\nZip Archive Filename: [$file_Zip]\n");
			system("$main::compress_7z a -r -mx9 \"$fileZipPathRetail$file_Zip\" $buildPathRetail/../*.*");
		}
		if ($hasClassic == 1) {
			my $file_Zip = "$filebaseCl.zip";
			if (-e "\"$fileZipPathClassic$file_Zip\""){
				unlink("$fileZipPathClassic$file_Zip") or warn "\nCould not delete [\"$fileZipPathClassic$file_Zip\"]: $!";
			}
			print("\nZip Archive Filename: [$file_Zip]\n");
			system("$main::compress_7z a -r -mx9 \"$fileZipPathClassic$file_Zip\" $buildPathClassic/../*.*");
		}
	}
}
if ($main::compress_rar ne ""){
	#RAR
	if ($main::makeRar == 1){
	
		if ($hasRetail == 1) {
			my $file_rar = "$filebase.rar";
			if (-e "\"$fileZipPathRetail$file_rar\""){
				unlink("\"$fileZipPathRetail$file_rar\"") or warn "\nCould not delete [\"$fileZipPathRetail$file_rar\"]: $!";
			}
			print("\nRAR Archive Filename: [$file_rar]\n");
			system("$main::compress_rar a -r -m5 -t \"$fileZipPathRetail$file_rar\" $buildPathRetail/../*.*");
		}
		if ($hasClassic == 1) {
			my $file_rar = "$filebaseCl.rar";
			if (-e "\"$fileZipPathClassic$file_rar\""){
				unlink("\"$fileZipPathClassic$file_rar\"") or warn "\nCould not delete [\"$fileZipPathClassic$file_rar\"]: $!";
			}
			print("\nRAR Archive Filename: [$file_rar]\n");
			system("$main::compress_rar a -r -m5 -t \"$fileZipPathClassic$file_rar\" $buildPathClassic/../*.*");
		}
	}
}

# Pause for input to let our users read the printed data.
print "\nProcess Completed.\n\n"